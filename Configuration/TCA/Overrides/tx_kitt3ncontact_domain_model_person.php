<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'kitt3n_contact',
   'tx_kitt3ncontact_domain_model_person'
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$sModel = basename(__FILE__, '.php');

// Icon
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_contact/Resources/Public/Icons/' . $sModel . '.svg';

// Backend labels
$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'lastname';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'firstname, position';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

// $GLOBALS['TCA'][$sModel]['columns']['streets']['config']['foreign_sortby'] = 'name';
//$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = str_replace('streets,', '', $GLOBALS['TCA'][$sModel]['types']['1']['showitem']);
// gender, title, firstname, middlename, lastname, image, description, address, city, zip, region, country, email, phone, mobile, fax, website, birthday, position, company, building, room


// Tabs and palettes
$GLOBALS['TCA'][$sModel]['types'][1] = [
    'showitem' => '
        --div--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.tab.person,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.person;person,
        --div--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.tab.address,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.address;address,       
        --div--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.tab.contact,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.contact;contact,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.organization;organization,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.social;social,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.building;building,
            --palette--;LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.palette.coordinates;coordinates,
                       
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;paletteHidden,
        --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category, categories
    '
];
// Palettes
$GLOBALS['TCA'][$sModel]['palettes'] = [
    'person' => [
        'showitem' => 'gender, title, --linebreak--,
                            firstname, middlename, lastname,--linebreak--,                          
                            birthday,--linebreak--,
                            description,--linebreak--,
                            image'
    ],
    'organization' => [
        'showitem' => 'position, company'
    ],
    'address' => [
        'showitem' => 'address, --linebreak--,
                            city, zip, region, --linebreak--,
                            country, --linebreak--,'
    ],
    'building' => [
        'showitem' => 'building, room'
    ],
    'coordinates' => [
        'showitem' => 'latitude,longitude'
    ],
    'contact' => [
        'showitem' => 'email, --linebreak--,
                            phone, mobile, fax, --linebreak--,
                            website'
    ],
    'social' => [
        'showitem' => 'facebook, instagram, --linebreak--,
                            snapchat, twitter, --linebreak--,
                            skype, --linebreak--,
                            xing, linkedin'
    ],
    'paletteHidden' => [
        'showitem' => '
                hidden, starttime, endtime
            ',
    ],
    'language' => ['showitem' => 'sys_language_uid, l10n_parent'],
];



// Column specifications
$GLOBALS['TCA'][$sModel]['columns']['gender'] = [
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.gender',
    'config' => [
        'type' => 'radio',
        'items' => [
            [ 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.gender.male', 1 ],
            [ 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.gender.female', 2 ],
            [ 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.gender.diverse', 3 ],
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['title'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.title',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 8,
        'max' => 255,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['firstname'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.firstname',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 255
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['middlename'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.middlename',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 255
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['lastname'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.lastname',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 255
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['description'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.description',
    'config' => [
        'type' => 'text',
        'rows' => 5,
        'cols' => 48,
        'softref' => 'typolink_tag,url',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['image']['label'] = 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.image';

$GLOBALS['TCA'][$sModel]['columns']['address'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.address',
    'config' => [
        'type' => 'text',
        'cols' => 20,
        'rows' => 3,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['city'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.city',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 255,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['zip'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.zip',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 10,
        'max' => 20,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['region'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.region',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 10,
        'max' => 255,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['country'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.country',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 128,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['email'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.email',
    'config' => [
        'type' => 'input',
        'eval' => 'email',
        'size' => 20,
        'max' => 255,
        'softref' => 'email',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['phone'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.phone',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 30,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['mobile'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.mobile',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 30,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['fax'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.fax',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 30,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['website'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.website',
    'config' => [
        'type' => 'input',
        'renderType' => 'inputLink',
        'fieldControl' => [
            'linkPopup' => [
                'options' => [
                    'blindLinkOptions' => 'mail,file,spec,folder',
                ],
            ],
        ],
        'eval' => 'trim',
        'size' => 20,
        'max' => 255,
        'softref' => 'typolink,url',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['birthday'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.birthday',
    'config' => [
        'type' => 'input',
        'renderType' => 'inputDateTime',
        'eval' => 'date,int',
        'default' => 0
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['position'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.position',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 255,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['company'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.company',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 255,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['building'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.building',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 40,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['room'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.room',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 40,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['latitude'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.latitude',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 20,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];
$GLOBALS['TCA'][$sModel]['columns']['longitude'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:kitt3n_contact/Resources/Private/Language/translation_db.xlf:tx_kitt3ncontact_domain_model_person.tca.column.longitude',
    'config' => [
        'type' => 'input',
        'eval' => 'trim',
        'size' => 20,
        'max' => 20,
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ],
];