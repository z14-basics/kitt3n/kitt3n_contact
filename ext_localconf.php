<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KITT3N.Kitt3nContact',
            'Kitt3ncontactpersonlist',
            [
                'Person' => 'list'
            ],
            // non-cacheable actions
            [
                'Person' => 'list'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    kitt3ncontactpersonlist {
                        iconIdentifier = kitt3n_contact-plugin-kitt3ncontactpersonlist
                        title = LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_contact_kitt3ncontactpersonlist.name
                        description = LLL:EXT:kitt3n_contact/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_contact_kitt3ncontactpersonlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = kitt3ncontact_kitt3ncontactpersonlist
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'kitt3n_contact-plugin-kitt3ncontactpersonlist',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kitt3n_contact/Resources/Public/Icons/user_plugin_kitt3ncontactpersonlist.svg']
			);
		
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function ($extKey, $globals) {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.kitt3ncontactpersonlist >
                wizards.newContentElement.wizardItems {
                    KITT3N {
                        header = KITT3N
                        after = common,special,menu,plugins,forms
                        elements {
                            kitt3ncontactpersonlist {
                                iconIdentifier = kitt3n_svg_big
                                title = kitt3n | Contact person list
                                description = Shows the selected contacts in a list.
                                tt_content_defValues {                                    
                                    CType = list
                                    list_type = kitt3ncontact_kitt3ncontactpersonlist
                                }
                            }
                        }
                        show := addToList(kitt3ncontactpersonlist)
                    }

                }
            }'
        );

        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
        $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] =
            \KITT3N\Kitt3nContact\Hooks\Previews\ContactListPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);