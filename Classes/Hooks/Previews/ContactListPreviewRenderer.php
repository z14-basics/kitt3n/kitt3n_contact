<?php

namespace KITT3N\Kitt3nContact\Hooks\Previews;

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Database\ConnectionPool;

class ContactListPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    )
    {

        if ($row['list_type'] !== 'kitt3ncontact_kitt3ncontactpersonlist') {
            return;
        } else {

            // ConfigurationManager initilisieren
            $this->configurationManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

            // parse xml from flexform to array
            $ffXml = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($row['pi_flexform']);

            // get oContacts
            $sSelectedPersons = $ffXml['data']['sDEF']['lDEF']['settings.sPersons']['vDEF'];

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_kitt3ncontact_domain_model_person');
            $statement = $queryBuilder
                ->select('firstname', 'lastname')
                ->from('tx_kitt3ncontact_domain_model_person')
                ->where(
                    $queryBuilder->expr()->in('uid', $sSelectedPersons)
                )
                ->execute();

            $sRows = '';
            $j = 1;
            while ($aRow = $statement->fetch()) {
                $sRows .=
                    '<tr>
                        <td style="padding: 10px 20px;">
                            <p style="margin: 0;"> ' . $aRow['firstname'] . ' ' . $aRow['lastname'] .'</p>
                        </td>                       
                    </tr>';

                $j++;
            }

            // show limit in preview
            $sDynamicContentPart = '';
            if($ffXml['data']['sDEF']['lDEF']['settings.sHeadline']['vDEF']){
                $sDynamicContentPart .= $ffXml['data']['sDEF']['lDEF']['settings.sHeadline']['vDEF'];
            }


            // preview html
            $sContentHtml = '            
            <div class="kitt3n_contacts">
                <strong style="display: block; margin-bottom: 20px;">'. $sDynamicContentPart .'</strong> 
                <table class="table table-condensed" style="padding: 20px 0; border: none;">
                    <tbody>            
                        '. $sRows .'                       
                    </tbody>
                </table>
            </div>            
            ';

            $itemContent = $sContentHtml;
            $drawItem = false;

        }
    }
}